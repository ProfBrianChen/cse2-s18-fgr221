/*
Griffin Reichert
3/10/18
Hw07 String analysis
*/
import java.util.Scanner; //imports scanner
public class StringAnalysis { //creates class
  public static boolean fullStringCheck(String input){ //method to check the full string
    int length = input.length();  //calculates length of the string
    int count = 0; //initializes count at 0
    for (int i = 0; i < length; i++){ //for loop that runs through all characters of the string
      char character = input.charAt(i); //parses to specific char
      boolean letter = false; //initializes boolean
      if ('a' <= character && character <= 'z'){letter = true;} //changes boolean to true if char is a letter
      if (letter == true){count++;} // for every letter, count increases by 1
    }
    if (count == length ){return (true);} //if every char is a letter, returns true
    else {return(false);} // if not every character is a letter, returns false
  }
  public static boolean partStringCheck(String input, int num){ //method to check a specific character of the string
    int count = 0; //initializes count at 0
    for (int i = 0; i < num; i++){ //for loop that runs through the specific number of characters
      char character = input.charAt(i);//parses to specific char
      boolean letter = false; //initializes boolean
      if ('a' <= character && character <= 'z'){letter = true;} //changes boolean to true if char is a letter
      if (letter == true){count++;} // for every letter, count increases by 1
    }
    if (count == num ){return (true);}  //if every char in the section is a letter, returns true
    else {return(false);} // else returns false if not all are letters
  }
  public static void main(String[] args){ //main method
    Scanner scanner = new Scanner(System.in); //creates scanner
    boolean letterCheck; // creates boolean
    int stringPos = 0;  // creates string pos
    System.out.println("Please enter a string:"); //prompts user to input a string 
    String input = scanner.nextLine(); // stores input string
    while (1==1){ //infinite loop
      System.out.println("Please enter '1' to examine the full string, '2' to examine a specific number of characters:"); //prompts user for type
      String type = scanner.nextLine();
      if (type.equals("1")){ // if user enters 1
        letterCheck = fullStringCheck(input); //runs fullStringCheck method with input string
        System.out.println("It is " + letterCheck + " that the string contains all letters."); // prints result
        break; // breaks loop
      }
      if (type.equals("2")){ //if user enters 2
        int lengthCheck = input.length(); //creates variable to check length of string
        while (1==1){ // infinite loop
          System.out.println("Please enter the number of characters you would like to check (input should be an integer):"); //asks for number of characters to check
          if (scanner.hasNextInt()){ // if input is an integer
            stringPos = scanner.nextInt(); //sets string pos equal to input integer
            break; // breaks loop
          }
          else {scanner.nextLine();} //clears scanner
        } // closes infinite loop
        if (lengthCheck < stringPos){ // if number of characters to check is greater than number of characters in the string
          letterCheck = fullStringCheck(input); // runs full string check method
          System.out.println("It is " + letterCheck + " that the string contains all letters."); // prints result
          break;
        }
        else { // if number of characters is less than string length
          letterCheck = partStringCheck(input, stringPos); // runs partial check method
          System.out.println("It is " + letterCheck + " that the section of " + stringPos + " characters contains all letters."); // prints result
          break; //breaks loop
        }
      }
    }// closes outer infinite loop
  }
}