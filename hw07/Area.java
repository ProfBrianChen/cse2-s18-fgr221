/*
Griffin Reichert
3/10/18
Hw07 Area
*/

import java.util.Scanner;  //imports scanner
public class Area { 	  //java class
  public static void rectangleArea(double width, double height){ // rectangle area method
    System.out.println("The are of the rectangle is: " + width * height);  //prints and calculates the area of the rectangle
  }
  public static void triangleArea(double base, double height){  // triangle area method
    System.out.println("The area of the triangle is: " + (.5 * base * height));  // prints and calculates the area of the triangle
  }
  public static void circleArea(double radius){  // circle area method
    System.out.println("The area of the circle is: " + (3.14 * radius * radius)); // prints and calculates the area of the circle
  }
  public static double inputMethod(String prompt){ // asks for input and checks that input is a double
    Scanner scanner = new Scanner(System.in); // creates scanner
    while (1 == 1){   // infinite loop
      System.out.println(prompt);  // promps the user to enter a double
      if (scanner.hasNextDouble()){  // checks that input was a double
        double input = scanner.nextDouble();  // if input was a double, assigns input to variable input
        return (input); // breaks loop and returns the value that was entered
      }
    scanner.nextLine();  // clears scanner
    }
  }
  public static void main(String[] args){ // main method
    Scanner scanner = new Scanner(System.in); // creates scanner
    while (1 == 1){ // infinite loop
      System.out.println("Please enter the shape you would like (rectangle, triangle, or circle):"); // prompts user for type of shape
      if (scanner.hasNext("rectangle")){ // if user enters rectangle
        String prompt1 = "Please enter the width of the rectangle as a double: "; // sets prompt string to ask for width
        double width = inputMethod(prompt1);// calls inputMethod using prompt1 to get a value for variable width
        String prompt2 = "Please enter the height of the rectangle as a double: "; // sets prompt string to ask for height
        double height = inputMethod(prompt2);  // calls inputMethod using prompt2 to get a value for variable height
        rectangleArea(width, height); // calls rectangleArea method to calculate and print width of rectangle
        break; // breaks infinite loop
      }
      if (scanner.hasNext("triangle")){ // if user enters triangle
        String prompt1 = "Please enter the base of the triangle as a double: "; // sets prompt string to ask for base
        double base = inputMethod(prompt1); // calls inputMethod using prompt1 to get a value for variable base
        String prompt2 = "Please enter the height of the triangle as a double: "; // sets prompt string to ask for height
        double height = inputMethod(prompt2); // calls inputMethod using prompt2 to get a value for variable height
        triangleArea(base, height);// calls triangleArea method to calculate and print width of triangle
        break; // breaks infinite loop
      }
      if (scanner.hasNext("circle")){ // if user enters circle
        String prompt1 = "Please enter the radius of the circle as a double: "; // sets prompt string to ask for radius
        double radius = inputMethod(prompt1); // calls inputMethod using prompt1 to get a value for variable radius
        circleArea(radius); // calls circleArea method to calculate and print the area of the circle
        break; // breaks infinite loop
      }
      scanner.nextLine(); //clears scanner
    } // closes infinite loop
  } // closes main method
} // closes class
