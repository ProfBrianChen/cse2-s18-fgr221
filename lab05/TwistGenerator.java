// Griffin Reichert
// Lab 5
// 3/2/18
// Twist Generator

import java.util.Scanner; //imports scanner tool
public class TwistGenerator {  // class for java programs
  public static void main(String[] args){ //main method
    Scanner myScanner = new Scanner(System.in); // declares myScanner
    System.out.println("Please enter a positive integer: "); //propts user for an integer
    boolean flag = true;  // initializes variable flag to be true
    int val = 0;         // initializes variable val to be 0;
    if (myScanner.hasNextInt()){  //checks if user entered an integer
      val = myScanner.nextInt();  //makes val equal to inputed integer
      if ( val < 0){      //checks if value is negative
        flag = false;     // if value is negative makes flag false
      }
    }
    else {
      flag = false; // declares flag as false if input is not an integer
    }
    
    while ( !flag ){  // runs while flag is false
      myScanner.nextLine();  //clears what is in the scanner
      System.out.println("Please enter a positive integer: "); //propts user for an integer
      if (myScanner.hasNextInt()){  //checks if user entered an integer
        val = myScanner.nextInt();  //makes val equal to inputed integer
        if ( val < 0){
          flag = false; // makes flag false if integer is negative
        }
        else {
          flag = true; // changes flag to true if positive integer was entered
        }
      } // closes outer if
    } //closes while loop 
    
    int twist = val / 3;
    int remainder = val % 3;
    
    for(int i = 0; i < twist; i++){ //creates a for loop for the number of twists
      System.out.print("\\ /");
    }
    if (remainder == 1){ // prints remaining characters if remainder is 1
      System.out.print("\\");
    }
    if (remainder == 2){ // prints remaining characters if remainder is 2
      System.out.print("\\ ");
    }
    System.out.print("\n"); // new line
    
    
    for(int i = 0; i < twist; i++){ //creates a for loop for the number of twists
      System.out.print(" X ");
    }
    if (remainder == 1){ // prints remaining characters if remainder is 1
      System.out.print(" ");
    }
    if (remainder == 2){ // prints remaining characters if remainder is 2
      System.out.print(" X");
    }
    System.out.print("\n"); // new line
    
    
    for(int i = 0; i < twist; i++){ //creates a for loop for the number of twists
      System.out.print("/ \\");
    }
    if (remainder == 1){ // prints remaining characters if remainder is 1
      System.out.print("/");
    }
    if (remainder == 2){ // prints remaining characters if remainder is 2
      System.out.print("/ ");
    }
    System.out.print("\n"); // new line
    

  } // closes main method
} // closes class