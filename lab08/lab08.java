/* 
Griffin Reichert
Lab 08
4/6/18
*/

import java.util.Scanner;
import java.util.Random;

public class lab08{
  public static void main(String[] args){
    Scanner scanner = new Scanner(System.in);
    Random random = new Random();
    int arraySize = random.nextInt(5)+5;
    String[] students = new String[arraySize];
    for (int i = 0; i < arraySize; i++){
      System.out.println("Please enter the student's name:");
      students[i] = scanner.nextLine();
    }
    int[] midterm = new int[arraySize];
    for (int i = 0; i < arraySize; i++){
      midterm[i] = random.nextInt(100);
    }
    for (int i = 0; i < arraySize; i++){
      System.out.println("\n"+students[i]+": "+midterm[i]);
    }
  }
}