// Griffin Reichert
// 2/2/18
// hw01

public class WelcomeClass {
  public static void main(String [] args) {
    //print welcome
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
    System.out.println("<-F--G--R--2--2--1->"); 
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    System.out.println(" ");
    //print autobiography
    System.out.println("My name is Griffin Reichert, I'm a freshman at Lehigh.");
    System.out.println("I'm from Jackson Hole Wyoming, and I love to ski, play golf, adventure, and play lacrosse.");
    System.out.println("Fun fact, I have met Arnold Schwarzenegger.");

  }
}
                       