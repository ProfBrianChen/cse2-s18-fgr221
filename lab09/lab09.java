/*
Griffin Reichert
Lab09
4/13/18
*/

import java.util.Scanner;

public class lab09{
  
  public static int[] copy(int[] array){
    int[] array2 = new int[array.length];
    for (int i = 0; i < array.length; i++){
      int val = array[i];
      array2[i] = val;
    }
    return array2;
  }
  
  public static void print(int[] array){
    for (int i = 0; i < array.length; i++){
      System.out.print(array[i]+" ");
    }
    System.out.println(" ");  
  }
  
  public static void inverter(int[] array){
    for (int i = 0; i < array.length/2; i++){
      int val = array[i];
      array[i] = array[array.length - i - 1];
      array[array.length - i - 1] = val;
    }
  }
  
  public static int[] inverter2(int[] array){
    int[] list = copy(array);
    for (int i = 0; i < list.length/2; i++){
      int val = list[i];
      list[i] = list[list.length - i - 1];
      list[list.length - i - 1] = val;
    }
    return list;
  }
  
  public static void main(String[] args){
    int[] array0 = {11, 22, 33, 44, 55, 66, 77, 88, 99};
    int[] array1 = copy(array0);
    int[] array2 = copy(array0);
    inverter(array0);
    System.out.println("Array0:");
    print(array0);
    inverter2(array1);
    System.out.println("Array1:");
    print(array1);
    int[] array3 = inverter2(array2);
    System.out.println("Array3:");
    print(array3);
  }
}