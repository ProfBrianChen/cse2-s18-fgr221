
import java.util.Scanner;
public class battleship{
  
  public static String[][] blankBoard(){
    String[][] array = new String[10][10];
    for(int i = 0; i < array.length; i++){
      for(int j = 0; j < array[i].length; j++){
        array[i][j] = ".";
      }
    }
    return array;
  }
  
  public static void print(String[][] array){
    System.out.println("  A  B  C  D  E  F  G  H  I  J ");
    for(int i = 0; i < array.length; i++){
      System.out.print(i);
      for(int j = 0; j < array[i].length; j++){
        if(array[i][j].equals("o")){
          System.out.print(" . ");
        }
        else{
          System.out.print(" " + array[i][j] + " ");
        }
      }
      System.out.print("\n");
    }
  }
  
  public static int colCoordinate(String input){
    char colChar = input.charAt(0);
    int col = 0;
    switch (colChar){
      case 'A':
        col = 0;
        break;
      case 'B':
        col = 1;
        break;
      case 'C':
        col = 2;
        break;
      case 'D':
        col = 3;
        break;
      case 'E':
        col = 4;
        break;
      case 'F':
        col = 5;
        break;
      case 'G':
        col = 6;
        break;
      case 'H':
        col = 7;
        break;
      case 'I':
        col = 8;
        break;
      case 'J':
        col = 9;
        break;
    }  //turns char into integer
    return col;
  }
  
  public static int rowCoordinate(String input){
    char rowChar = input.charAt(1);
    int row = 0;
    switch (rowChar){
      case '0':
        row = 0;
        break;
      case '1':
        row = 1;
        break;
      case '2':
        row = 2;
        break;
      case '3':
        row = 3;
        break;
      case '4':
        row = 4;
        break;
      case '5':
        row = 5;
        break;
      case '6':
        row = 6;
        break;
      case '7':
        row = 7;
        break;
      case '8':
        row = 8;
        break;
      case '9':
        row = 9;
        break;
    }  //turns char into integer
    return row;
  }
  
  public static int placeShips(String[][] board){
    Scanner scanner = new Scanner(System.in);
    int hitCount = 0;
    System.out.println("Enter the number of ships you want to place: ");
    int numOfShips = scanner.nextInt();
    scanner.nextLine();
    for(int i = 0; i < numOfShips; i++){
      System.out.println("Please enter the coordinate of one end of the ship (ex: D7):");
      String input = scanner.nextLine();
      int intColPos = colCoordinate(input);
      int intRowPos = rowCoordinate(input);
      System.out.println("Please enter the coordinate of the other end of the ship (ex: D3):");
      input = scanner.nextLine();
      int endColPos = colCoordinate(input);
      int endRowPos = rowCoordinate(input);
      
      int colChange = endColPos - intColPos;
      int rowChange = endRowPos - intRowPos;
      
      if(0 < colChange && colChange < 6 && rowChange == 0){
        for(int j = intColPos; j <= endColPos; j++){
          board[intRowPos][j] = "o";
          hitCount++;
        }
      }
      if(0 < rowChange && rowChange < 6 && colChange == 0){
        for(int j = intRowPos; j <= endRowPos; j++){
          board[j][intColPos] = "o";
          hitCount++;
        }
      }
      else if((rowChange == 0 && colChange == 0) || (rowChange > 0 && colChange >0)){
        System.out.println("Error, coordinates do not create valid ship!");
        break;
      }
    } 
    return hitCount;
  }
  
  public static int shoot(String[][] board, int hitCount){
    Scanner scanner = new Scanner(System.in);
    System.out.println("\nEnter the coordinate to shoot:");
    String input = scanner.nextLine();
    int col = colCoordinate(input);
    int row = rowCoordinate(input);
    String boardVal = board[row][col];
    if(boardVal.equals("o")){
      board[row][col] = "X";
      hitCount++;
    }
    if(boardVal.equals(".")){
      board[row][col] = "x";
    }
    return hitCount;
  }
  
//   public static boolean continueGame(){
    
//   }
  
  
  public static void main(String[] args){
    System.out.println("\n   Welcome to Battleship!\n");
    boolean quit = false;
    String[][] board = blankBoard(); //[row][column]
    print(board);
    int totalHits = placeShips(board); //places ships
    print(board);
    int hitCount = 0;
    do{
      hitCount = shoot(board, hitCount);
      print(board);
    }while(hitCount < totalHits);
    System.out.println("\nGame Over!");
  }
}