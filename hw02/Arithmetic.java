public class Arithmetic {
  public static void main(String [] args) {
  //INPUT VARIABLES  
    //Number of pairs of pants
    int numPants = 3;
    double pantsPrice = 34.98;   //Cost per pair of pants

    //Number of sweatshirts
    int numShirts = 2;
    double shirtPrice = 24.99;   //Cost per sweatshirt

    //Number of belts
    int numBelts = 1;
    double beltPrice = 33.99;     //cost per belt
 
    //the tax rate
    double paSalesTax = 0.06;    //pennsylvania sales tax
    
  //DECLARE VARIABLES
    //total cost of each item before tax
    double totalCostOfPants;
    double totalCostOfShirts;  //total cost of sweatshirts 
    double totalCostOfBelts;
    double totalBeforeTax;    //total cost without tax
    
    // other variables to store taxes are declared later
    //total cost of order
    double totalCost;
    
    //Calculating cost of each type of item
    totalCostOfPants = numPants * pantsPrice;
    totalCostOfShirts = numShirts * shirtPrice;
    totalCostOfBelts = numBelts * beltPrice;

    totalBeforeTax = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
    
    //calculating tax on each item (to two deciaml places typecast into ints)
    int pantsTax = (int) (paSalesTax * totalCostOfPants * 100);
    int shirtsTax = (int) (paSalesTax * totalCostOfShirts *100);
    int beltsTax = (int) (paSalesTax * totalCostOfBelts * 100);
    // convert back to a two digit decimal
    double taxOnPants = pantsTax/100.0;
    double taxOnShirts = shirtsTax/100.0;
    double taxOnBelts = beltsTax/100.0;
    //declare variable for total tax
    double taxTotal = taxOnPants + taxOnShirts + taxOnBelts;
    

    
    //calculating total cost
    totalCost = taxTotal + totalBeforeTax;
    
    
    // print numbers
    System.out.println("The total cost of pants is: $" + totalCostOfPants);
    System.out.println("The total cost of sweatshirts is: $" + totalCostOfShirts);
    System.out.println("The total cost of belts is: $" + totalCostOfBelts);
    System.out.println("The total tax on pants is: $" + taxOnPants);
    System.out.println("The total tax on sweatshirts is: $" + taxOnShirts);
    System.out.println("The total tax on belts is: $" + taxOnBelts);    
    System.out.println("The total cost before tax is: $" + totalBeforeTax);
    System.out.println("The total sales tax is: $" + taxTotal);
    System.out.println("The total cost is: $" + totalCost);
 
	}
}