/*
Griffin Reichert
3/10/18
Hw06 Argyle
*/

import java.util.Scanner;  //imports scanner
public class Argyle{ 	  //java class
  public static void main(String [] args) { //main method
    Scanner scanner = new Scanner(System.in); // declares scanner
    int count = 0;				//creates a counter variable
    int numCheck = 0;			//creates a varibale to temporarily store inputs
    int windowWidth = 0;				//creates a variable to store width of box
    int windowHeight = 0;				//creates a variable to store height of box
    int diamondWidth = 0;			//creates a variable to store width of diamond
    int stripe = 0;				//creates a variable to store width of stripe
    String stringCheck = " ";  //creates a variable to temporarily store inputs
    String fillPattern = " ";	//creates a variable to store character for background
    String diamondPattern = " "; //creates a variable to store character for diamonds
    String stripePattern = " "; //creates a variable to store character for stripes

    ///////////////////////
    // PROMPT FOR INPUTS //
    ///////////////////////
    
    while (count == 0){  
      System.out.println("Please enter a positive integer for the width of the window: "); //prompts for user input
      if (scanner.hasNextInt()){			//checks input is an integer
        numCheck = scanner.nextInt();	
        if ( 0 < numCheck){				//checks input is positive
          windowWidth = numCheck;
          count = 1;					//increases count
        }
      }
      scanner.nextLine();		// clears scanner
    } //window width
    while (count == 1){
      System.out.println("Please enter a positive integer for the height of the window: "); //prompts for user input
      if (scanner.hasNextInt()){			//checks input is an integer
        numCheck = scanner.nextInt();	
        if ( 0 < numCheck){				//checks input is positive
          windowHeight = numCheck;
          count = 2;        //increases count
        }
      }
      scanner.nextLine();		// clears scanner
    } //window height
    while (count == 2){
      System.out.println("Please enter a positive integer for the width of the argyle diamonds: "); //prompts for user input
      if (scanner.hasNextInt()){			//checks input is an integer
        numCheck = scanner.nextInt();	
        if ( 0 < numCheck){				//checks input is positive
          diamondWidth = numCheck;
          count = 3;					//increases count
        }
      }
      scanner.nextLine();		// clears scanner
    } //diamond width
    while (count == 3){
      System.out.println("Please enter a positive odd integer for the width of the argyle stripes: "); //prompts for user input
      if (scanner.hasNextInt()){			//checks input is an integer
        numCheck = scanner.nextInt();	
        if ((0 < numCheck) && (numCheck % 2 == 1)){				//checks input is positive
          stripe = numCheck;
          count = 4;					//increases count
        }
      }
      scanner.nextLine();		// clears scanner
    } //stripe width
    while (count == 4){
      System.out.println("Please enter a character for the background pattern: ");  //propts user for the background character
      stringCheck = scanner.next();     //assigns next string to designated variable
      if ( stringCheck.length() == 1){  //ensures string length is equal to 1
        fillPattern = stringCheck;      
        count = 5;                      //increases count
      }
      scanner.nextLine();     //clears scanner
    } //background pattern
    while (count == 5){
      System.out.println("Please enter a character for the diamond pattern: ");  //propts user for the diamond character
      stringCheck = scanner.next();     //assigns next string to designated variable
      if ( stringCheck.length() == 1){  //ensures string length is equal to 1
        diamondPattern = stringCheck;      
        count = 6;                      //increases count
      }
      scanner.nextLine();     //clears scanner
    } //diamond pattern  
    while (count == 6){
      System.out.println("Please enter a character for the stripe pattern: ");  //propts user for the stripe character
      stringCheck = scanner.next();     //assigns next string to designated variable
      if ( stringCheck.length() == 1){  //ensures string length is equal to 1
        stripePattern = stringCheck;      
        count = 7;                      //increases count
      }
      scanner.nextLine();     //clears scanner
    } //stripe pattern
    
    //////////////////////////
    // PRINT ARGYLE PATTERN //
    //////////////////////////

    int completeDiamondWidth = 0;        //creates variable for complete number of diamonds that fit in the box
    int diamondRemainderWidth = 0;       //creates variable for remaining number of characters to print
    int completeDiamondHeight = 0;        //creates variable for complete number of diamonds that fit in the box
    int diamondRemainderHeight = 0;       //creates variable for remaining number of characters to print
    
    completeDiamondWidth = windowWidth / (2 * diamondWidth); 
    diamondRemainderWidth = windowWidth % (2 * diamondWidth);
    
    completeDiamondHeight = windowHeight / (2 * diamondWidth);
    diamondRemainderHeight = windowHeight % (2 * diamondWidth);
    
    System.out.print("\n");
    System.out.println("complete diamonds (x): " + completeDiamondWidth);
    System.out.println("remaining characters (x): " + diamondRemainderWidth);
    System.out.println("complete diamonds (y): " + completeDiamondHeight);
    System.out.println("remaining characters (y): " + diamondRemainderHeight);
    System.out.print("\n");
    
    for (int h = 0; h < completeDiamondHeight; h++){ // complete diamonds by height
      for (int i = 0; i < (2 * diamondWidth); i++){ // number of rows
        for(int d = 0; d < completeDiamondWidth; d++){ // complete diamonds by width
          for (int j = 0; j < (2 * diamondWidth); j++){ // prints characters
           if (i == j){
              System.out.print(stripePattern);  //prints stripe pattern
            }
            else if ((stripe > 1) && ( i - (stripe -2) == j )){ //prints stripe pattern
              System.out.print(stripePattern);
            }
            else if ((stripe > 1) && ( i == j - (stripe - 2))){ //prints stripe pattern
              System.out.print(stripePattern);
            }
            else if ((i + j) == (2 * diamondWidth - (stripe - 2))){ //prints stripe pattern
              System.out.print(stripePattern);
            }
            else if ((stripe > 1) && ((i+j) == (2 * diamondWidth - (stripe - 3)))){ //prints stripe pattern
              System.out.print(stripePattern);
            }
            else if ((stripe > 1) && ((i+j) == (2 * diamondWidth - (stripe - 1)))){ //prints stripe pattern
              System.out.print(stripePattern);
            }
            else if ((((i+j) >= (diamondWidth - 1)) && ((i+j) < (3 * diamondWidth ))) && (((j-i) <= (diamondWidth)) && ((j-i) >= (-1*diamondWidth)))) {
              System.out.print(diamondPattern); // prints diamond pattern
            }
            else {
              System.out.print(fillPattern); // prints fill pattern
            }
          } // j loop
        } // d loop

        for (int r = 0; r < diamondRemainderWidth; r++){ //prints remaining characters of the diamonds
            if (i == r){
              System.out.print(stripePattern);  //prints stripe pattern
            }
            else if ((stripe > 1) && ( i - (stripe -2) == r )){ //prints stripe pattern
              System.out.print(stripePattern);
            }
            else if ((stripe > 1) && ( i == r - (stripe - 2))){ //prints stripe pattern
              System.out.print(stripePattern);
            }
            else if ((i + r) == (2 * diamondWidth - (stripe - 2))){ //prints stripe pattern
              System.out.print(stripePattern);
            }
            else if ((stripe > 1) && ((i+r) == (2 * diamondWidth - (stripe - 3)))){ //prints stripe pattern
              System.out.print(stripePattern);
            }
            else if ((stripe > 1) && ((i+r) == (2 * diamondWidth - (stripe - 1)))){ //prints stripe pattern
              System.out.print(stripePattern);
            }
            else if ((((i+r) > (diamondWidth - 1)) && ((i+r) < (3 * diamondWidth ))) && (((r-i) <= (diamondWidth)) && ((r-i) >= (-1*diamondWidth)))) {
              System.out.print(diamondPattern); // prints diamond pattern
            }
            else {
              System.out.print(fillPattern); // fill pattern
            }
          } // r loop

        System.out.print("\n");
      } // i loop 
    }
    for (int i = 0; i < diamondRemainderHeight; i++){ // for loop for remaining characters in terms of height
        for(int d = 0; d < completeDiamondWidth; d++){ // prints number of complete diamonds
          for (int j = 0; j < (2 * diamondWidth); j++){ // prints characters
            if (i == j){ //prints stripe pattern
              System.out.print(stripePattern);
            }
            else if ((stripe > 1) && ( i - (stripe -2) == j )){ //prints stripe pattern
              System.out.print(stripePattern);
            }
            else if ((stripe > 1) && ( i == j - (stripe - 2))){ //prints stripe pattern
              System.out.print(stripePattern);
            }
            else if ((i + j) == (2 * diamondWidth - (stripe - 2))){ //prints stripe pattern
              System.out.print(stripePattern);
            }
            else if ((stripe > 1) && ((i+j) == (2 * diamondWidth - (stripe - 3)))){ //prints stripe pattern
              System.out.print(stripePattern);
            }
            else if ((stripe > 1) && ((i+j) == (2 * diamondWidth - (stripe - 1)))){ //prints stripe pattern
              System.out.print(stripePattern);
            }
            else if ((((i+j) >= (diamondWidth - 1)) && ((i+j) < (3 * diamondWidth ))) && (((j-i) <= (diamondWidth)) && ((j-i) >= (-1*diamondWidth)))) {
              System.out.print(diamondPattern); //prints diamond
            }
            else {
              System.out.print(fillPattern); //print fill
            }
          } // j loop
        } // d loop

        for (int r = 0; r < diamondRemainderWidth; r++){ //prints remaining characters of the diamonds
            if (i == r){    //prints stripe pattern
              System.out.print(stripePattern);
            }
            else if ((stripe > 1) && ( i - (stripe -2) == r )){ //prints stripe pattern
              System.out.print(stripePattern);
            }
            else if ((stripe > 1) && ( i == r - (stripe - 2))){ //prints stripe pattern
              System.out.print(stripePattern);
            }
            else if ((i + r) == (2 * diamondWidth - (stripe - 2))){ //prints stripe pattern
              System.out.print(stripePattern);
            }
            else if ((stripe > 1) && ((i+r) == (2 * diamondWidth - (stripe - 3)))){ //prints stripe pattern
              System.out.print(stripePattern);
            }
            else if ((stripe > 1) && ((i+r) == (2 * diamondWidth - (stripe - 1)))){ //prints stripe pattern
              System.out.print(stripePattern);
            }
            else if ((((i+r) >= (diamondWidth - 1)) && ((i+r) < (3 * diamondWidth ))) && (((r-i) <= (diamondWidth)) && ((r-i) >= (-1*diamondWidth)))) {
              System.out.print(diamondPattern); //prints diamond
            }
            else {
              System.out.print(fillPattern); //prints fill
            }
          } // r loop

        System.out.print("\n"); // new line
    }
    System.out.print("\n");
  } // main method
}  // class
