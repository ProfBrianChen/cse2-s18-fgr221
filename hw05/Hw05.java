/*
Griffin Reichert
3/5/18
Hw 06
*/

/* 
ask for the course number, department name, the number of times it meets in a week,
the time the class starts, the instructor name, and the number of students.
*/

import java.util.Scanner; //imports scanner tool
public class Hw05 {        //main class 
  public static void main(String[] args){    //main method for java programs
    Scanner myScanner = new Scanner( System.in );  //declares new scanner
    
    // Department Name //
    String dept = " ";       //creates string variable to store department name
    boolean flag1 = true;    //creates boolean named flag and initializes as true
    System.out.println("Please enter the department name (ex: for CSE2 enter CSE): "); // prompts user to input course department
    if ( myScanner.hasNext("[a-zA-Z]+") ){ // checks to see if input is a string containing letters a-z
      dept = myScanner.nextLine();  // if input is a string, sets string equal to the inputed department name
    }
    else {
      flag1 = false;  //makes flag false if user did not input a string
    }
    while ( !flag1 ){  // runs while flag is false
      myScanner.nextLine(); // clears the scanner
      System.out.println("Please enter the department name as a string (ex: for CSE2 enter CSE): "); // prompts user to input course department
      if ( myScanner.hasNext("[a-zA-Z]+") ){  // checks to see if input is a string containing letters a-z
        dept = myScanner.nextLine();  //sets string equal to the inputed department name
        flag1 = true;                 //sets flag to true to break out of loop
      }
    } // closes while
    
    // Course Number //
    int num = 0;        //creates integer variable num to store course number    
    boolean flag2 = true;    //creates boolean named flag and initializes as true
    System.out.println("Please enter your course number (ex: for CSE2, enter 2): ");  //prompts user to input their course number
    if (myScanner.hasNextInt()){  //checks if user entered an integer
      num = myScanner.nextInt();  //makes course number equal to inputed number
    }
    else {
      flag2 = false;  //makes flag false if user did not input an integer
    }
    while ( !flag2 ){ // while flag is false
      myScanner.nextLine();  // clears what is in the scanner      
      System.out.println("Please enter your course number (ex: for CSE2, enter 2): ");  //prompts user to input their course number
      if (myScanner.hasNextInt()){  //if input was an integer
        num = myScanner.nextInt(); // declares course number to be number that is input by user
        flag2 = true;  // breaks out of while loop after an integer is entered
      }
    } //closes while

    // Meetings per week //
    int week = 0;       //creates integer variable week to store number of meetings a week    
    boolean flag3 = true;    //creates boolean named flag and initializes as true
    System.out.println("Please enter the number of meetings per week: ");  //prompts user to input number of meetings per week
    if (myScanner.hasNextInt()){  //checks if user entered an integer
      week = myScanner.nextInt();  //makes meetings per week equal to inputed number
    }
    else {
      flag3 = false;  //makes flag false if user did not input an integer
    }
    while ( !flag3 ){ // while flag is false
      myScanner.nextLine();  // clears what is in the scanner      
      System.out.println("Please enter the number of meetings per week as an integer: ");  //prompts user to input number of weeks
      if (myScanner.hasNextInt()){ // checks if input is an integer
        week = myScanner.nextInt(); //makes meetings per week equal to inputed number
        flag3 = true;  // breaks out of while loop after an integer is entered
      }
    } //closes while
    
    // Class Times //
    String time = " ";      //creates string variable time to store class times    
    boolean flag4 = true;    //creates boolean named flag and initializes as true
    System.out.println("Please enter the time class meets (ex: for 8:45 enter 8:45): ");  //prompts user to input class time
    if (myScanner.hasNext("[0-9]+:[0-9]+")){  //checks if user entered a string in the form 00:00
      time = myScanner.nextLine();  //makes time equal to inputed string
    }
    else {
      flag4 = false;  //makes flag false if user did not input a string in the correct form
    }
    while ( !flag4 ){ // while flag is false
      myScanner.nextLine();  // clears what is in the scanner      
      System.out.println("Please enter the time class meets as a string (ex: for 8:45 enter 8:45): ");  //prompts user to input class time
      if (myScanner.hasNext("[0-9]+:[0-9]+")){ // checks string again
        time = myScanner.nextLine(); // makes class time equal to input string 
        flag4 = true;  // breaks out of while loop after a string is entered
      }
    } //closes while
    
    // Number of students //
    int students = 0;     //creates integer variable week to store number of students    
    boolean flag5 = true;    //creates boolean named flag and initializes as true
    System.out.println("Please enter the number of students in the class: ");  //prompts user to input number of students
    if (myScanner.hasNextInt()){  //checks if user entered an integer
      students = myScanner.nextInt();  //makes number of students equal to inputed number
    }
    else {
      flag5 = false;  //makes flag false if user did not input an integer
    }
    while ( !flag5 ){ // while flag is false
      myScanner.nextLine();  // clears what is in the scanner      
      System.out.println("Please enter the number of students in the class as an integer: ");  //prompts user to input number of students
      if (myScanner.hasNextInt()){
        students = myScanner.nextInt(); // makes number of students equal to input integer 
        flag5 = true;  // breaks out of while loop after an integer is entered
      }
    } //closes while
    
    // Instructor Name //
    String instructorName = " "; //creates string variable instructorName to store instructors name
    boolean flag6 = true;    //creates boolean named flag and initializes as true
    System.out.println("Please enter the instructor's name: "); // prompts user to input name of instructor
    if ( myScanner.hasNext("[a-zA-Z]+") ){ // checks to see if input is not a string with letters
      instructorName = myScanner.nextLine();  // sets instructor name to input string
    }
    else {
      flag6 = false;  //makes flag false if user did not input a string
    }
    while ( !flag6 ){
      myScanner.nextLine(); // clears the scanner
      System.out.println("Please enter the instructor's name as a string of letters (ex: professor chen): "); // prompts user to input name of instructor
      if ( myScanner.hasNext("[a-zA-Z]+") ){  // checks to see if input is not a string with letters
        instructorName = myScanner.nextLine();  //sets string equal to the inputed department name
        flag6 = true; //sets flag to true to break loop
      }
    } // closes while
    
  } // closes method
} // closes class
