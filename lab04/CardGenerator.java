//Griffin Reichert
//lab 04
//card generator
//2/16/18

public class CardGenerator{           // main method required for every Java program
  public static void main(String[] args) {
    int cardNum = (int)(Math.random()*(52+1))+1;   //generates a random number between 1 and 52
    String card = " ";          //creates a string to store the card that is picked
    String suit = " ";          //creates a string to store the suit of the card
    
    if ( cardNum >= 1 && cardNum <=13 ){    //assigns diamons to cards 1-13
      suit = "diamonds";
    }
    else if ( cardNum >= 14 && cardNum <=26 ){ //assigns clubs to cards 14-26
      suit = "clubs";
    }
    else if ( cardNum >= 27 && cardNum <=39 ){  // assigns hears to cards 27-39
      suit = "hearts";
    }
    else if ( cardNum >= 40 && cardNum <=52 ){ //assigns spades to cards 40-52
      suit = "spades";
    }
    int newCardNum = (int) cardNum % 13;    //divides card number by 13, newCardNum is the remainder, which allows us to find the card number no matter the suit
    switch (newCardNum){          //switch statement determines what card it is depending on the newCardNum
      case 1:
        card = "ace";
        break;
      case 2:
        card = "2";
        break;       
      case 3:
        card = "3";
        break;       
      case 4:
        card = "4";
        break;       
      case 5:
        card = "5";
        break;       
      case 6:
        card = "6";
        break;       
      case 7:
        card = "7";
        break;       
      case 8:
        card = "8";
        break;       
      case 9:
        card = "9";
        break;       
      case 10:
        card = "10";
        break;       
      case 11:
        card = "jack";
        break;       
      case 12:
        card = "queen";
        break;       
      case 0:
        card = "king";
        break;
    }
    System.out.println( "You picked the " + card + " of " + suit + "." );  //combines suit and card to print the card that was picked to the user
  }
}