//Griffin Reichert
//2/9/18
//hw03 Pyramid

import java.util.Scanner;     //tell java to search for Scanner tool so that it can be used in the program
public class Convert{           // main method required for every Java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );   //declare the scanner
    System.out.print("Enter the affected area in acres (xxxxx.xx): ");
    double acres = myScanner.nextDouble();
    System.out.print("Enter the ammount of rain in inches: ");
    double rainInches = myScanner.nextDouble();
    double squareMiles;
    double rainMiles;
    double cubicMiles;
    squareMiles = acres / 640.00;
    rainMiles = rainInches * 0.0000157828;
    cubicMiles = rainMiles * squareMiles;
    System.out.println(cubicMiles + " cubic miles of rain");
  }
}
    
