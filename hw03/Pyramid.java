//Griffin Reichert
//2/9/18
//hw03 pyramid 

import java.util.Scanner;     //tell java to search for Scanner tool so that it can be used in the program

public class Pyramid{           // main method required for every Java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );   //declare the scanner
    System.out.print("The square side of the pyramid is: "); //prompt for the side length of the pyramid
    double sideLength = myScanner.nextDouble();             //declare side length as next imput
    System.out.print("The height of the pyramid is: ");   //prompt for the side length of the pyramid
    double height = myScanner.nextDouble();               //declare height as the next input
    double volume;                                        //declare volume
    volume = (sideLength * sideLength) * (height / 3);             //calculate volume of pyramid
    System.out.println("The volume of the pyramid is: " + volume); //print out the final volume
  }
}