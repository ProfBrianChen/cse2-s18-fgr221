// Lab02 
// Griffin Reichert
// 2/2/18

//this cyclometer program prints out the time(s) and distance(miles) traveled during trips on a bike

public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {

      //Create variables
      int secsTrip1=480;  //duration of trip 1 in seconds
      int secsTrip2=3220;  //duration of trip 2 in seconds
		  int countsTrip1=1561;  //number of wheel rotations in trip 1
		  int countsTrip2=9037; //number of wheel rotations in trip 1
      double wheelDiameter=27.0,  //diameter of the wheel of the bike
      PI=3.14159, //variable to hold the value of pi
      feetPerMile=5280,  //conversion factor of number of feet in a mile
      inchesPerFoot=12,   //conversion factor of number of inches in a foot
      secondsPerMinute=60;  //number of seconds in a minute
      double distanceTrip1, distanceTrip2,totalDistance;  //declares variable to store distance of trip 1, 2, and the total distance
      
      //Print out the variables
      System.out.println("Trip 1 took "+
        (secsTrip1/secondsPerMinute)+" minutes and had "+
        countsTrip1+" counts.");
      System.out.println("Trip 2 took "+
        (secsTrip2/secondsPerMinute)+" minutes and had "+
        countsTrip2+" counts.");
      
      //Calculate the distances
      distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
      distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
      distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; //calculates distance in trip 2
      totalDistance=distanceTrip1+distanceTrip2; // adds trip 1 and 2 to find total distance

      //Print out the output data.
      System.out.println("Trip 1 was "+distanceTrip1+" miles");
      System.out.println("Trip 2 was "+distanceTrip2+" miles");
      System.out.println("The total distance was "+totalDistance+" miles");

      
	}  //end of main method   
} //end of class
