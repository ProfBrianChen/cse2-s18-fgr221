/*
Griffin Reichert
4/20/18
Lab 10  */

import java.util.Random;
public class lab10{
  public static int[][] increasingMatrix(int width, int height, boolean format){
    int count = 1;
    System.out.println("Generating array with height "+height+" and width "+width);
    if (format == true){                 //ROW format
      int [][] array = new int[height][width];
      for(int r = 0; r < height; r++){
        for(int c = 0; c < width; c++){
          array[r][c] = count++;
        }
      }
      return array;
    }
    if (format == false){                //COLUMN format
      int [][] array = new int[height][width];
      for(int c = 0; c < width; c++){
        for(int r = 0; r < height; r++){
          array[r][c] = count++;
        }
      }
      return array;
    }
    return null;
  }
  
  public static void print(int[][] array, boolean format){    //Print method
    if(format == true){
      for(int r = 0; r < array.length; r++){
        System.out.print("[ ");
        for(int c = 0; c < array[r].length; c++){
          System.out.print(array[r][c] + " ");
        }
        System.out.print("]\n");
      }
      System.out.print("\n");
    }
    if(format == false){            //prints a column major array to look like a row major array
      for(int c = 0; c < array.length; c++){
        System.out.print("[ ");
        for(int r = 0; r < array[r].length; r++){
          System.out.print(array[r][c] + " ");
        }
        System.out.print("]\n");
      }
      System.out.print("\n");
    }
  }
  
  public static int[][] translate(int[][] array){
    int[][] row = new int[array.length][array[0].length];
    for(int r = 0; r < row.length; r++){
      for(int c = 0; c < row[r].length; c++){
        row[r][c] = array[r][c];
      }
    }
    return row;
  }
  
  public static void addMatrix(int[][] matrixA, boolean A, int[][] matrixB, boolean B){
    if (A != B){
      int[][] newMatrix = new int[matrixA.length][matrixA[0].length];
      for(int r = 0; r < newMatrix.length; r++){
        for(int c = 0; c < newMatrix[r].length; c++){
          int val1 = matrixA[r][c];
          int val2 = matrixB[r][c];
          newMatrix[r][c] = (val1+val2);
        }
      }
      boolean k = true;
      print(newMatrix, k);
    }
    else{
      System.out.println("Unable to add input matricies!");
    }
  }
  
  public static void main(String[] args){
    Random random = new Random();
    int width = random.nextInt(5)+2;
    int height  = random.nextInt(5)+2;
    boolean rowFormat = true;
    boolean colFormat = false; 
    int [][] matrixA = increasingMatrix(width, height, rowFormat); //runs method to create row major matrix
    print(matrixA, rowFormat);
    int [][] matrixB = increasingMatrix(width, height, colFormat); //runs method to create col major matrix
    matrixB = translate(matrixA);
    print(matrixB, rowFormat);
    width = random.nextInt(7)+1;
    height  = random.nextInt(7)+1;
    int [][] matrixC = increasingMatrix(width, height, rowFormat); //new matrix
    print(matrixC, rowFormat);
    System.out.println("Adding arrays A & B: ");
    addMatrix(matrixA, rowFormat, matrixB, colFormat);
    System.out.println("Adding arrays A & C: ");
    addMatrix(matrixA, rowFormat, matrixC, rowFormat);
  }
}