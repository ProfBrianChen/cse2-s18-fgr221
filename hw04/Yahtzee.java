//Griffin Reichert
//CSE2 Brian Chen
//hw04 Yahtzee
//2/16/18

import java.util.Scanner;

public class Yahtzee{           // main method required for every Java program
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner( System.in );   //declare the scanner
    
    int ones = 0; //creates variables for the number of ones, twos, threes, etc, that are rolled
    int twos = 0;
    int threes = 0;
    int fours = 0;
    int fives = 0;
    int sixes = 0;
    
    int dice1 = 0;  // creates a variable for the face value of each dice. 
    int dice2 = 0;
    int dice3 = 0;
    int dice4 = 0;
    int dice5 = 0;
    
    System.out.println("\n      Welcome to Yahtzee!");
    System.out.println("Option 1: Random dice roll");
    System.out.println("Oprion 2: Manually enter dice roll");
    System.out.println("Please enter the number of the option you would like to choose (ex: 1): ");
    int gameMode = myScanner.nextInt();       // creates variable game mode to store which option the user chooses to play
    int diceInput;                            //creates a varibale for the manually imputed dice values

    if ( gameMode == 1 ){
      dice1 = (int) (Math.random()*(6)+1);    //randomly rolls each of 5 dice (possible values from 0-6)//
      dice2 = (int) (Math.random()*(6)+1);    
      dice3 = (int) (Math.random()*(6)+1);    
      dice4 = (int) (Math.random()*(6)+1);    
      dice5 = (int) (Math.random()*(6)+1);    
    }
    else if( gameMode == 2 ){
      // prompt the user to imput a number for the dice roll
      System.out.println("Please enter the dice roll you would like as a 5 digit number (ex: 12345): ");   
      diceInput = myScanner.nextInt(); // record the number the user types in
      if ( diceInput >= 11111 && diceInput <= 66666){  
        dice1 = (diceInput / 10000) % 10;  // the first digit of the number is the value of dice 1
        dice2 = (diceInput / 1000) % 10;   // the second digit of the number is the value of dice 2
        dice3 = (diceInput / 100) % 10;    // the third digit of the number is the value of dice 3
        dice4 = (diceInput / 10) % 10;     // the fourth digit of the number is the value of dice 4
        dice5 = diceInput % 10;            // the fifth digit of the number is the value of dice 5

        //checks that the input dice are within the bounds of possible values
        if ( (0 >= dice1 || 0 >= dice2 || 0 >= dice3 || 0 >= dice4 || 0 >= dice5) || (dice1 >= 7 || dice2 >= 7 || dice3 >= 7 || dice4 >= 7 || dice5 >= 7 )){
          System.out.println("\n ERROR!"); //prints an error message to the screen
          dice1 = 0;  // if it is an error, set all dice to 0
          dice2 = 0;
          dice3 = 0;
          dice4 = 0;
          dice5 = 0;  
        }//end of check
      }
      else {      //execute if dice input was not a possible number
        System.out.println("\n ERROR!"); 
        dice1 = 0;
        dice2 = 0;
        dice3 = 0;
        dice4 = 0;
        dice5 = 0;
      } //end of else
    }
    else {      //execute if game mode input was not a 1 or a 2
      System.out.println("\n ERROR!"); 
      dice1 = 0;
      dice2 = 0;
      dice3 = 0;
      dice4 = 0;
      dice5 = 0;
    }
    
    
    System.out.println("\nDice 1 is a: " + dice1);  //prints out the value of each dice//
    System.out.println("Dice 2 is a: " + dice2);  //
    System.out.println("Dice 3 is a: " + dice3);  //
    System.out.println("Dice 4 is a: " + dice4);  //
    System.out.println("Dice 5 is a: " + dice5);  //
    System.out.println(" ");
    
    switch (dice1){   //this counts the number rolled by dice number 1//
      case 1:
        ones++;    // if the dice rolls a one, increase the number of ones rolled by one
        break;
      case 2:
        twos++;    // if the dice rolls a two, increase the number of twos rolled by one
        break;
      case 3:
        threes++;    // if the dice rolls a three, increase the number of threes rolled by one
        break;
      case 4:
        fours++;    // if the dice rolls a four, increase the number of fours rolled by one
        break;
      case 5:
        fives++;    // if the dice rolls a five, increase the number of fives rolled by one
        break;
      case 6:
        sixes++;   // if the dice rolls a six, increase the number of sixes rolled by one
        break;
    }
    
    switch (dice2){   //this counts the number rolled by dice number 2//
      case 1:
        ones++;    // if the dice rolls a one, increase the number of ones rolled by one
        break;
      case 2:
        twos++;    // if the dice rolls a two, increase the number of twos rolled by one
        break;
      case 3:
        threes++;    // if the dice rolls a three, increase the number of threes rolled by one
        break;
      case 4:
        fours++;    // if the dice rolls a four, increase the number of fours rolled by one
        break;
      case 5:
        fives++;    // if the dice rolls a five, increase the number of fives rolled by one
        break;
      case 6:
        sixes++;   // if the dice rolls a six, increase the number of sixes rolled by one
        break;
    }
    
    switch (dice3){   //this counts the number rolled by dice number 3//
      case 1:
        ones++;    // if the dice rolls a one, increase the number of ones rolled by one
        break;
      case 2:
        twos++;    // if the dice rolls a two, increase the number of twos rolled by one
        break;
      case 3:
        threes++;    // if the dice rolls a three, increase the number of threes rolled by one
        break;
      case 4:
        fours++;    // if the dice rolls a four, increase the number of fours rolled by one
        break;
      case 5:
        fives++;    // if the dice rolls a five, increase the number of fives rolled by one
        break;
      case 6:
        sixes++;   // if the dice rolls a six, increase the number of sixes rolled by one
        break;
    }
    
    switch (dice4){   //this counts the number rolled by dice number 4//
      case 1:
        ones++;    // if the dice rolls a one, increase the number of ones rolled by one
        break;
      case 2:
        twos++;    // if the dice rolls a two, increase the number of twos rolled by one
        break;
      case 3:
        threes++;    // if the dice rolls a three, increase the number of threes rolled by one
        break;
      case 4:
        fours++;    // if the dice rolls a four, increase the number of fours rolled by one
        break;
      case 5:
        fives++;    // if the dice rolls a five, increase the number of fives rolled by one
        break;
      case 6:
        sixes++;   // if the dice rolls a six, increase the number of sixes rolled by one
        break;
    }
    
    switch (dice5){   //this counts the number rolled by dice number 5//
      case 1:
        ones++;    // if the dice rolls a one, increase the number of ones rolled by one
        break;
      case 2:
        twos++;    // if the dice rolls a two, increase the number of twos rolled by one
        break;
      case 3:
        threes++;    // if the dice rolls a three, increase the number of threes rolled by one
        break;
      case 4:
        fours++;    // if the dice rolls a four, increase the number of fours rolled by one
        break;
      case 5:
        fives++;    // if the dice rolls a five, increase the number of fives rolled by one
        break;
      case 6:
        sixes++;   // if the dice rolls a six, increase the number of sixes rolled by one
        break;
    }
    
//     System.out.println(ones);   //prints out the value of each dice//
//     System.out.println(twos);   
//     System.out.println(threes); 
//     System.out.println(fours);  
//     System.out.println(fives);  
//     System.out.println(sixes); 
//     System.out.println(" ");

    // Calculate Upper Section initial total //
    
    int upperSectionTotal = (ones * 1) + (twos * 2) + (threes * 3) + (fours * 4) + (fives * 5) + (sixes * 6); 
    
    if (upperSectionTotal >= 63){          // If the upper section total is 63 or greater, give a bonus of 35 points //
      upperSectionTotal =  upperSectionTotal + 35;
    }
    
    /*
    Calculate lower section total
    */
    
    int lowerSectionTotal = 0;  //creates variable to store the lower section total score
    int grandTotal = 0;         //creates variable to store the grand total score
    
    if ( ones == 5 || twos == 5  || threes == 5 || fours == 5 || fives == 5 || sixes == 5 ){  // conditions for a Yahtzee
      System.out.println("YAHTZEE!");     // print the result of the roll
      lowerSectionTotal = 50;             // give the player a bonus of 50 points for scoring a yahtzee
    }
    if ( ( ones > 0 && twos > 0 && threes > 0 && fours > 0 ) || ( twos > 0 && threes > 0 && fours > 0 && fives > 0 ) || ( threes > 0 && fours > 0 && fives > 0 && sixes > 0 ) ){ //large straight
      System.out.println("Large Straight!");     // print the result of the roll
      lowerSectionTotal = 40;                    // give the player a 40 point bonus for a large straight
    }
    else if ( ( ones > 0 && twos > 0 && threes > 0 ) || ( twos > 0 && threes > 0 && fours > 0 ) || ( threes > 0 && fours > 0 && fives > 0) || ( fours > 0 && fives > 0 && sixes > 0 ) ){ // small straight
      System.out.println("Small Straight!");    // print the result of the roll
      lowerSectionTotal = 30;                   // give the player a 30 point bonus for rolling a small straight
    }
    else if ( ones == 3 || twos == 3  || threes == 3 || fours == 3 || fives == 3 || sixes == 3 ){  // conditions for a three of a kind
      if ( ones == 2 || twos == 2  || threes == 2 || fours == 2 || fives == 2 || sixes == 2 ){
        System.out.println("Full House!");
        lowerSectionTotal = 25;
      } //end full house
      else {
        System.out.println("Three of a kind!");     // print the result of the roll
        lowerSectionTotal = upperSectionTotal;      // a three of a kind is scored as the sum of all dice, this is the same as the upper section total
      } //end three of a kind
    }
    else if ( ones == 4 || twos == 4  || threes == 4 || fours == 4 || fives == 4 || sixes == 4 ){  // conditions for a four of a kind
      System.out.println("Four of a kind!");      // print the result of the roll
      lowerSectionTotal = upperSectionTotal;      // a four of a kind is scored as the sum of all dice, this is the same as the upper section total
    }  
    else {  // scores as a chance if lower section meets none of the other conditions
      lowerSectionTotal = upperSectionTotal; //chance is the sum of the dice, and is the same as the upper section total
    }
    
    // calculate & print grand total
    grandTotal = lowerSectionTotal + upperSectionTotal;   //the grand total is the sum of the lower & upper section totals
    System.out.println("The upper section total is: " + upperSectionTotal);     //print the upper section total
    System.out.println("The lower section total is: " + lowerSectionTotal);  //print the lower section total
    System.out.println("The grand total is: " + grandTotal);  //print the grand total
    
 }
}
                       