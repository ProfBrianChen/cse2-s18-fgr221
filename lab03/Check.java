//Griffin Reichert
//Lab03
//2/9/18


import java.util.Scanner;     //tell java to search for Scanner tool so that it can be used in the program
public class Check{           // main method required for every Java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );   //declare the scanner
    System.out.print("Enter the original cost of the check in the form xx.xx: "); //prompt the user for the cost of the check
    double checkCost = myScanner.nextDouble();                                    //create a variable for the cost of the check (input by user)
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); //prompt the user for the tip
    double tipPercent = myScanner.nextDouble();                                   //create a variable for the tip percentage (input by user)
    tipPercent /= 100; //convert the percentage into a decimal value
    System.out.print("Enter the number of people who went out to dinner: "); //prompt the user for the number of people
    int numPeople = myScanner.nextInt();                 //create variable for number of people (input by user)
    double totalCost;                                      
    double costPerPerson;
    int dollars, dimes, pennies;                         
    totalCost = checkCost * (1 + tipPercent);            //calculates tip cost
    costPerPerson = totalCost / numPeople;               //calculates cost per person
    dollars=(int)costPerPerson;                          //get the whole amount, dropping decimal fraction
    dimes=(int)(costPerPerson * 10) % 10;                // gets the dimes amount
    pennies=(int)(costPerPerson * 100) % 10;             // gets the pennies ammount
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies); //prints the final cost per person

  }  //end of main method   
} //end of class
