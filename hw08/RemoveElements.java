/*
Griffin Reichert
4/7/18
HW 8
*/

import java.util.Scanner;   //imports scanner
import java.util.Random;    //imports random
public class RemoveElements{  //class
  public static String listArray(int num[]){ 
    String out="{";
    for(int j=0;j<num.length;j++){
      if(j>0){
        out+=", ";
      }
      out+=num[j];
    }
    out+="} ";
    return out;
  }
  
  public static int[] randomInput(){ //method for random input
    Random rnd = new Random();
    int[] list = new int[10];
    for(int i = 0; i<10; i ++){
      list[i] = rnd.nextInt(10); //sets value of array equal to random int 0-9
    }
    return list; //returns array
  }
    
  public static int[] delete(int[] list, int index){
    int[] list2 = new int[list.length-1];
    boolean indexCheck = true;
    if ( 0 > index || index > 9){
      indexCheck = false;
    }
    if (indexCheck == false){
      System.out.println("The index is not valid.");
      list2 = list;
    }
    else if(indexCheck == true){
      for(int i = 0; i < list.length -1; i++){
        if(i < index){
          list2[i] = list[i];
        }
        else{
          list2[i] = list[i+1];
        }
      }
    }
    return list2;
  }
  
  public static int[] remove(int list[],int target){ //remove method
    int count = 0;
    for (int i = 0; i < 10; i++){
      if (list[i] == target){
        count++;
      }
    }
    System.out.println("The target appears " + count + " times");
    int check = 0; 
    int index = 0; 
    int[] list3 = new int[list.length - count]; 
    while (check < list.length && index < list3.length){ 
      if(list[check] != target){ 
        list3[index] = list[check]; 
        check++;
        index++;
      } 
      else {
        for(int k = check; k < list.length; k++){
           if(list[k] == target){ 
              k++; 
           }
           else{ 
              k = list.length; 
           }
        }           
      }
    }
    return list3; //return list 3
  }
  
  public static void main(String[] args){ //main
    Scanner scan=new Scanner(System.in);
    int num[]=new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
    String answer="";
    do{
      System.out.print("Random input 10 ints [0-9]");
      num = randomInput();
      String out = "The original array is:";
      out += listArray(num);
      System.out.println(out);
      System.out.print("Enter the index ");
      index = scan.nextInt();
      newArray1 = delete(num,index);
      String out1="The output array is ";
      out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);
      System.out.print("Enter the target value ");
      target = scan.nextInt();
      newArray2 = remove(num,target);
      String out2="The output array is ";
      out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
      System.out.println(out2);
      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
      answer=scan.next();
    }while(answer.equals("Y") || answer.equals("y"));
  }//main

      
    
}//class