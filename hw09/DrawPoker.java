/*
Griffin Reichert
HW09
Poker
4/14/18
*/

import java.util.Scanner;  //imports scanner
public class DrawPoker{    //main class         
  
  public static int[] copy(int[] array){          //copys integer array
    int[] array2 = new int[array.length];         //makes a new array of same length
    for (int i = 0; i < array.length; i++){       //copys each value of array into array2
      int val = array[i];
      array2[i] = val;                            //makes value of second array a copy of what the value is in the first array
    } 
    return array2;                                //returns array2
  }
  
  public static void sort(int[] array){           //Method to sort arrays
    for (int i = array.length-1; i >= 1; i--){    //sorts all terms in array
      int max = array[0];
      int maxIndex = 0;
      for(int j = 1; j <= i; j++){                //finds maximum term up to i
        if(max < array[j]){
          max = array[j];
          maxIndex = j;
        }
      }
      if(maxIndex != i){                          //if i is not max term, changes value of i to be the max term
        array[maxIndex] = array[i];
        array[i] = max;
      }
    }
  }
  
  public static void shuffle(int[] array){        //method to shuffle deck array
    for(int i = 0; i < array.length; i++){        
      int random = (int)((array.length)*Math.random()); //random integer between 0 & 51
      int val = array[random];                    //stores value of array[random]
      array[random] = array[i];                   //makes array[random] equal to a new val, that was previously array[i]
      array[i] = val;                             //sets array[i] to the value of random term
    }
  }
  
  public static void filterCard(int[] array){
    for (int i = 0; i < array.length; i++){
      int val = array[i];
      array[i] = (val%13);
    }
  }

  public static void print(int[] array){          //method to print an array
    for(int i = 0; i < array.length; i++){        
      System.out.print(array[i]+" ");             //prints each member of array
    }
    System.out.println("\n");
  }
  
  public static int[] manualInput(){              //method for manual input of hands, returns integer array
    Scanner scanner = new Scanner(System.in);
    int[] hand = new int[5];
    for (int i = 0; i < 5; i++){
      int x = scanner.nextInt();
      if ((x>=0) && (x <= 51)){
        hand[i] = x;
      }
      else{
        System.out.println("ERROR! Please re-enter an integer within the range.");
        i--;
      }
    }
    return hand;
  }
  
  public static int[] deal5(int[] array, int playerNum){ //method to deal 5 cards to each player
    int[] hand = new int[5];                      //new array for hand of 5 cards
    int dealCount = playerNum;                    //indicates if player is dealt to first (0) or second (1)
    for(int i = 0; i < 5; i++){                   //fro loop to deal player 5 cards
      int cardVal = array[dealCount];             
      hand[i] = cardVal;                          //copies term in deck array to hand array
      dealCount += 2;                             //increases card number by 2 to deal player sequential cards (ie. 1, 3, 5, 7, 9 etc)
    }
    return hand;                                  //returns the hand player was dealt
  }
  
  public static boolean pair(int[] hand){         //method to evaluate pair
    boolean pair = false;
    for (int i = 0; i < 4; i++){
      for(int j = i+1; j < 5; j++){
        if(hand[i]%13 == hand[j]%13){
          pair = true;
          break;
        }
      }//j
    }//i
    return pair;
  }//pair method

  public static boolean threeOfKind(int[] hand){  //method to evaluate three of a kind
    boolean triple = false;
    for (int i = 0; i < 3; i++){
      for(int j = i+1; j < 4; j++){
        for(int k = i+2; k < 5; k++){
          if((hand[i]%13 == hand[j]%13) &&(hand[i]%13 == hand[k]%13)){
            triple = true;
            break;
          }
        }//k
      }//j
    }//i
    return triple;
  }
 
  public static boolean fullHouse(int[] hand){    //method to evaluate full house (assuming hand is sorted)
    int mid = 3;
    boolean fullHouse = false;
    if((hand[0] == hand[2]) && (hand[3] == hand[4])){ //if triple comes before pair
      fullHouse = true;
    }
    else if((hand[0] == hand[1]) && (hand[2] == hand[4])){ //if pair comes before triple
      fullHouse = true;
    }
    return fullHouse;                             //returns if player scored a full house
  }
  
  public static boolean flush(int[] hand){        //method to evaluate flush
    int suitVal = (int)(hand[0]/13);
    boolean flush = true;
    for(int i = 1; i < 5; i++){
      if((int)(hand[i]/13) != suitVal){
        flush = false;
        break;
      }
    }
    return flush;
  }

  public static int score(boolean fullHouse, boolean flush, boolean triple, boolean pair){ //method to give each hand the max score
    int score = 0;
    if (fullHouse == true){ score = 50; } 
    else if (flush == true){ score = 40; } 
    else if (triple == true){ score = 30; } 
    else if (pair == true){ score = 20; } 
    return score;
  }
  
  public static String scoreResult(int score){
    String scoreResult = " ";
    switch (score){
      case 20: scoreResult = "pair"; break;
      case 30: scoreResult = "three of a kind"; break;
      case 40: scoreResult = "flush"; break;
      case 50: scoreResult = "full house"; break;
    }
    return scoreResult;
  }
  
  public static int highCard(int[] hand1, int[] hand2){ //method to evaluate high card
    int highCard = 0;
    if(hand1[4] > hand2[4]){
      highCard = 1;
    }
    else if (hand1[4] < hand2[4]){
      highCard = 2;
    }
    return highCard;
  } 
  
  public static boolean runAgain(){
    Scanner scanner = new Scanner(System.in);
    boolean b = false;
    System.out.println("\nType 'Y' to play again, anything else to quit:");
    if (scanner.hasNext("Y") || scanner.hasNext("y")){
      b = true;
    }
    return b;
  }
  
  //      X      //
  // MAIN METHOD //
  //      X      //
  
  public static void main(String[] args){         
    Scanner scanner = new Scanner( System.in );   //declare the scanner
    System.out.println("\n\tWelcome to 5 Card Draw Poker!");
    boolean again = false;
    do{
      System.out.println("Enter a 1 if you wish to have a random shuffle, or a 2 if you wish to input each hand:");
      int gameMode;
      while(1==1){ 
        if(scanner.hasNextInt()){
          gameMode = scanner.nextInt();
          break;
        }
        System.out.println("ERROR! Enter a 1 or a 2 only.");
      }
      int[] hand1 = new int[5];
      int[] hand2 = new int[5];
      if (gameMode == 1){
        int[] deck = new int[52];                   //creates deck of 52 cards
        for (int i = 0; i < deck.length; i++){
          deck[i] = i;
        }
        int[] shuffledDeck = copy(deck);            //makes a copy of the deck
        shuffle(shuffledDeck);                      //randomizes the order of the deck to "shuffle" it
        hand1 = deal5(shuffledDeck, 0);             //calls deal method to deal player1 5 cards
        hand2 = deal5(shuffledDeck, 1);             //calls deal method to deal player2 5 cards
      }
      else if (gameMode == 2){
        System.out.println("Please enter 5 integers between 0 - 51 for hand one:");
        hand1 = manualInput();
        System.out.println("Please enter 5 integers between 0 - 51 for hand two:");
        hand2 = manualInput();
      }
      System.out.println("Player 1's hand is:");    //print
      print(hand1);                                 //calls method to print players hand
      System.out.println("Player 2's hand is:");    //print
      print(hand2);                                 //calls method to print players hand
      boolean P1flush = flush(hand1);               //creates boolean to tell whether hand has a flush
      boolean P1pair = pair(hand1);                 //creates boolean to tell whether hand has a pair
      boolean P1triple = threeOfKind(hand1);        //creates boolean to tell whether hand has three of a kind
      filterCard(hand1);
      sort(hand1);
      boolean P1fullHouse = fullHouse(hand1);       //creates boolean to tell whether hand has a full house
      boolean P2flush = flush(hand2);               //creates boolean to tell whether hand has a flush
      boolean P2pair = pair(hand2);                 //creates boolean to tell whether hand has a pair
      boolean P2triple = threeOfKind(hand2);        //creates boolean to tell whether hand has three of a kind
      filterCard(hand2);
      sort(hand2);
      boolean P2fullHouse = fullHouse(hand2);       //creates boolean to tell whether hand has a full house
      int score1 = score(P1fullHouse, P1flush, P1triple, P1pair); //determines best score in the hand
      int score2 = score(P2fullHouse, P2flush, P2triple, P2pair); //determines best score in the hand
      if (score1 == 0 && score2 == 0){
        int highCardResult = 0;
        highCardResult = highCard(hand1, hand2);
        if (highCardResult == 0){
          System.out.println("The result of the hand is a high card tie.");
        }
        if (highCardResult == 1){
          System.out.println("Player 1 wins with the high card!");
        }
        if (highCardResult == 2){
          System.out.println("Player 2 wins with the high card!");
        }
      }
      else{
        String result = " ";
        System.out.println("Player 1's Score: "+score1+"\nPlayer 2's Score: "+score2+"\n"); 
        if(score1 > score2){
          result = scoreResult(score1);
          System.out.println("Player 1 wins with a "+result);
        }
        if(score1 < score2){
          result = scoreResult(score2);
          System.out.println("Player 2 wins with a "+result);
        }
        else if (score1 == score2){
          System.out.println("The result of the hand is a tie.");
        }
      }
      again = runAgain();
    }while(again == true);
  }//MAIN
}//CLASS