/*
Griffin Reichert
hw10 Robot City
4/23/17
*/

import java.util.Random;
public class RobotCity{
  public static int[][] buildCity(){
    Random random = new Random();
    int northSouthDimension = random.nextInt(6)+10; //randomizes number of city blocks running from north to south
    int eastWestDimension = random.nextInt(6)+10;   //randomizes number of city blocks running from east to west
    System.out.println("\nWelcome to your city!");
    System.out.println("North-South: "+northSouthDimension+"\tEast-West: "+eastWestDimension+"\n");
    int[][] cityArray = new int[northSouthDimension][eastWestDimension]; //new array of specified dimensions
    for (int NS = 0; NS < cityArray.length; NS++){      //number of rows
      for (int EW = 0; EW < cityArray[0].length; EW++){ //members of each row
        int population = random.nextInt(900)+100;       //randomly sets population of each block to an integer between 100-999
        cityArray[NS][EW] = population;
      }
    }
    return cityArray;
  }
  
  public static void display(int[][] array){ //method to print array
    for(int i = 0; i < array.length; i++){
      for(int j = 0; j < array[i].length; j++){
        int val = array[i][j];
        System.out.printf("%5d",val);        //uses print with format to format all numbers in spreadsheet format
      }
      System.out.print("\n");
    }
    System.out.print("\n");
  }
  
  public static void invade(int[][] array, int robotTotal){ //method to invade the city with robots
    Random random = new Random();  
    int robotCount = 0;
    int roboPerRow = (int)(robotTotal/array.length);  //esimates how many robots should go in each row
    System.out.println("OH NO, ROBOTS ARE INVADING!");
    for(int i = 0; i < array.length; i++){
        for(int roboInRow = 0; roboInRow <= roboPerRow; roboInRow++){  //puts recomended number of robots into each row
          boolean flag = false;
          if(robotCount < robotTotal){      //makes sure number of robots that invaded are less than the total number allowed
            do{
              int r = random.nextInt(array[0].length); //random location for robot to land
              int val = array[i][r];
              if( val > 0 ){            //checks that block has not been previously invaded
                array[i][r] = val*(-1); //makes block negative to indicate presence of a robot
                flag = true;
                robotCount++;
              }
            }while( flag == false );    //runs until robot is placed
          }
      }
    }
//    System.out.println("Landed: "+robotCount);
  }//method
  
  public static void update(int[][] array){   //method to update invasion
    for(int u = 0; u < 5; u++){           //runs and prints 5x
      for(int i = 0; i < array.length; i++){  //runs through all columns
        for(int j = array[i].length - 1; j > 0; j--){ //moves right to left through each term in the row
          int prev = array[i][j-1];       //value of block to the left of current block
          int val = array[i][j];          //value of current block
          if (val < 0 && prev < 0){       //checks if back to back negative values
            array[i][j-1] = prev*(-1);
            j--;
          }
          else{             //if not a double negative
            if(prev < 0){   //if previous term is negative
              array[i][j] = val*(-1);
            } 
            if(val < 0){    //if current term is negative
              array[i][j] = val*(-1);
            }
          }
        }//for j
        if(u == 0){     //gets rid of negatives in first column of array since 0-1 = -1 which is out of bounds
          if( array[i][0] < 0){
            array[i][0] = array[i][0]*(-1);
          }
        }
      }//for i
      System.out.println("Update "+(u+1)+":");
      display(array);   //prints invaded array
    }//for u
    
  }//method
  
  public static void main(String[] args){ //main method
    Random random = new Random();
    int[][] cityArray = buildCity(); //builds city
    display(cityArray);
    int robots = random.nextInt(10)+20; //randomizes number of robots
    invade(cityArray, robots); //invades city
  //  System.out.println("Number of invading robots: "+robots+"\n");
    display(cityArray); //displays invaded city
    update(cityArray); //updates invasion
    
  }
}