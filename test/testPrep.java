
import java.util.Scanner;
import java.util.Random;
public class testPrep{
  
  public static void binarySearch(int[] array, int val){
    int count = 0;
    int low = 0;
    int high = array.length-1;
    while (low <= high){
      count++;
      int mid = (low + high)/2;
      if( val < array[mid] ){
        high = mid - 1;
      }
      if ( val > array[mid] ){
        low = mid + 1;
      }
      if ( val == array[mid] ){
        System.out.println("\nFound "+ val + " in "+ count +" iterations.");
        break;
      }
    }
    if( low > high){
      System.out.println("\nDid not find "+ val + " in "+ count +" iterations.");
    }
  }
  public static void main(String[] args){
    Scanner scanner = new Scanner(System.in);
    Random rand = new Random();
    int[] array = new int[11];
    array[0] = 1;
    for(int i = 1; i < array.length; i++){
      boolean flag = false;
      do{
        int num = rand.nextInt(10)*i-2;
        if (array[i-1] < num){
          array[i] = num;
          flag = true;
        }
      }while(flag == false);
    }
    System.out.print("[");
    for(int i = 0; i < array.length; i++){
      System.out.print(" "+array[i]);
    }
    System.out.println(" ]\n");
    int searchNum = 0;
    while(1==1){
      System.out.println("Enter a number to search for: ");
      if(scanner.hasNextInt()){
        searchNum = scanner.nextInt();
        break;
      }
    }
    binarySearch(array, searchNum);
  }
}