/*
Griffin Reichert
3/5/18
Hw 06
*/

/* 
ask for the course number, department name, the number of times it meets in a week,
the time the class starts, the instructor name, and the number of students.
*/

import java.util.Scanner; //imports scanner tool
public class Hw05 {        //main class 
  public static void main(String[] args){    //main method for java programs
    Scanner myScanner = new Scanner( System.in );  //declares new scanner
    boolean flag = true;                           //creates boolean named flag and initializes as true
    
    // Department Name //
    String dept = " ";
    System.out.println("Please enter the department name (ex: for CSE2 enter CSE): "); // prompts user to input course department
    if ( (myScanner.hasNextInt()) || (myScanner.hasNextDouble()) || (myScanner.hasNextFloat())){ // checks to see if input is not a string
      flag = false;  //makes flag false if user did not input a string
    }
    else {
      dept = myScanner.nextLine();  // if not an integer, sets string equal to the inputed department name
    }
    while ( !flag ){
      myScanner.nextLine(); // clears the scanner
      System.out.println("Please enter the department name (ex: for CSE2 enter CSE): "); // prompts user to input course department
      if ( (!myScanner.hasNextInt()) && (!myScanner.hasNextDouble()) && (!myScanner.hasNextFloat())){
        dept = myScanner.nextLine();  //sets string equal to the inputed department name
        flag = true; 
      }
    } // closes while
    
    // Course Number //
    int num = 0;                                   //creates integer variable num to store course number    
    flag = true;
    System.out.println("PLease enter your course number (ex: for CSE2, enter 2): ");  //prompts user to input their course number
    if (myScanner.hasNextInt()){  //checks if user entered an integer
      num = myScanner.nextInt();  //makes course number equal to inputed number
    }
    else {
      flag = false;  //makes flag false if user did not input an integer
    }
    while ( !flag ){ // while flag is false
      myScanner.nextLine();  // clears what is in the scanner      
      System.out.println("PLease enter your course number (ex: for CSE2, enter 2): ");  //prompts user to input their course number
      if (myScanner.hasNextInt()){
        num = myScanner.nextInt(); // declares course number to be number that is input by user
        flag = true;  // breaks out of while loop after an integer is entered
      }
    } //closes while

    // Meetings per week //
    int week = 0;                                   //creates integer variable week to store number of meetings a week    
    System.out.println("PLease enter the number of meetings per week: ");  //prompts user to input number of meetings per week
    if (myScanner.hasNextInt()){  //checks if user entered an integer
      week = myScanner.nextInt();  //makes meetings per week equal to inputed number
    }
    else {
      flag = false;  //makes flag false if user did not input an integer
    }
    while ( !flag ){ // while flag is false
      myScanner.nextLine();  // clears what is in the scanner      
      System.out.println("PLease enter the number of meetings per week: ");  //prompts user to input number of weeks
      if (myScanner.hasNextInt()){
        week = myScanner.nextInt(); //makes meetings per week equal to inputed number
        flag = true;  // breaks out of while loop after an integer is entered
      }
    } //closes while
    
    // Class Times //
    int time = 0;                                   //creates integer variable week to store class times    
    System.out.println("PLease enter the time class meets as an integer (ex: for 8:45 enter 845): ");  //prompts user to input class time
    if (myScanner.hasNextInt()){  //checks if user entered an integer
      time = myScanner.nextInt();  //makes meetings per week equal to inputed number
    }
    else {
      flag = false;  //makes flag false if user did not input an integer
    }
    while ( !flag ){ // while flag is false
      myScanner.nextLine();  // clears what is in the scanner      
      System.out.println("PLease enter the time class meets as an integer (ex: for 8:45 enter 845): ");  //prompts user to input class time
      if (myScanner.hasNextInt()){
        time = myScanner.nextInt(); // makes class time equal to input integer 
        flag = true;  // breaks out of while loop after an integer is entered
      }
    } //closes while
    
    // Number of students //
    int students = 0;                                   //creates integer variable week to store number of students    
    System.out.println("PLease enter the number of students in the class: ");  //prompts user to input number of students
    if (myScanner.hasNextInt()){  //checks if user entered an integer
      students = myScanner.nextInt();  //makes number of students equal to inputed number
    }
    else {
      flag = false;  //makes flag false if user did not input an integer
    }
    while ( !flag ){ // while flag is false
      myScanner.nextLine();  // clears what is in the scanner      
      System.out.println("PLease enter the number of students in the class: ");  //prompts user to input number of students
      if (myScanner.hasNextInt()){
        students = myScanner.nextInt(); // makes number of students equal to input integer 
        flag = true;  // breaks out of while loop after an integer is entered
      }
    } //closes while
    
    // Instructor Name //
    String instructorName = " ";
    System.out.println("Please enter the instructor's name: "); // prompts user to input name of instructor
    if ( (myScanner.hasNextInt()) || (myScanner.hasNextDouble()) || (myScanner.hasNextFloat())){ // checks to see if input is not a string
      flag = false;  //makes flag false if user did not input a string
    }
    else {
      instructorName = myScanner.nextLine();  // if not an integer, sets string equal to the inputed department name
    }
    while ( !flag ){
      myScanner.nextLine(); // clears the scanner
      System.out.println("Please enter the instructor's name: "); // prompts user to input name of instructor
      if ( (!myScanner.hasNextInt()) && (!myScanner.hasNextDouble()) && (!myScanner.hasNextFloat())){
        instructorName = myScanner.nextLine();  //sets string equal to the inputed department name
        flag = true; 
      }
    } // closes while
    
   System.out.println("Your class is " + dept  + num + ". It meets " + week + " times per week at " + time + " with " + instructorName + " and " + students + " students."); 
//     System.out.println("name is " + instructorName); 
  } // closes method
} // closes class
