public class occupied{
  public static void print(int[] x){
    int count = 0;
    for(int i = 0; i < x.length; i++){
      if( x[i] > 0){
        count++;
      }
    }
    int[] o = new int[count];
    for(int k = 0; k < o.length; k++){
      for(int i = 0; i < x.length; i++){
        if(x[i] > 0){
          o[k] = x[i];
          x[i] = 0;
          break;
        }
      }
    }
    System.out.print("[ ");
    for(int i = 0; i < o.length; i++){
      System.out.print(o[i]+" ");
    }
    System.out.println("]");
  }
  
  public static void main(String[] args){
    int[] y = { 1, 0, 3, 6, 7, 0, 0, 0, 420 };
    print(y);
  }
}