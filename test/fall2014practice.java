import java.util.Scanner;
public class fall2014practice{
  
  /////////////////
  //  problem 4  //
  /////////////////
  
  /*
  public static void pad(String[] strings){
    int length = strings.length;
    int max = 0;
    for(int i = 0; i < length; i++){
      int x = strings[i].length();
      if(x > max){
        max = x;
      }
    }
    for(int i = 0; i < length; i++){
      int y = strings[i].length();
      if(y < max){
        while(y < max){
          strings[i]+=" ";
          y = strings[i].length();
        }
      }
    }
  }
  public static void main(String [] args){
    String thai[] = {"one", "fine", "evening"};
    pad(thai);
    System.out.print("{");
    for(int i = 0; i < thai.length; i++){
      System.out.print(thai[i]+",");
    }
    System.out.print("}\n");
  }  */
  
  //////////////////
  //  problem 5   //
  //////////////////
  /*
  public static boolean ragged(int[][][] a){
    int size = a[0].length;
    //check if slabs are equal
    for(int slab = 1; slab < a.length; slab++){
      if(a[slab].length != size){
        return true; //ragged (not equal sizes)
      }
    }
    size = a[0][0].length;
    for(int slab = 0; slab < a.length; slab++){
      for(int row = 0; row < a[slab].length; row++){
        if(a[slab][row].length != size){
          return true;
        }
      }
    }
    return false;
  }
  
  public static void main(String[] args){
    int[][][] v ={
      {{2,2,2}, {3,3,3}, {1,1,1}, {4,4,4}},
      {{7,7,7}, {7,7,7}, {7,7,7}, {7,7,7}},
    };
    boolean x = ragged(v);
    if(x == false){
      System.out.println("kachow");
    }
    else{
      System.out.println("take a lap");
    }
  }
  */
  
  //////////////////
  //  problem 6   //
  //////////////////
  /*
  public static int[][] clockwise(int[][] a){
    int[][] b = new int[a[0].length][a.length];
    for(int i = 0; i < a.length; i++){
      for(int j = 0; j < a[i].length; j++){
        int val = a[i][j];
        b[j][i] = val;
      }
    }
    for(int i = 0; i < b.length; i++){
      for(int j = 0; j < b[i].length/2; j++){
        int val = b[i][j];
        b[i][j] = b[i][b[i].length - j - 1];
        b[i][b[i].length - j - 1] = val;
      }
    }
        
    return b;
  }
  public static void print(int[][] a){
    for(int i = 0; i < a.length; i++){
      System.out.print("{");
      for(int j = 0; j < a[i].length; j++){
        System.out.print(a[i][j]+" ");
      }
      System.out.print("}\n");
    }
  }
  public static void main(String [] args){
    int[][] a = {
      {0, 1, 2},
      {3, 4, 5},
      {6, 7, 8},
      {9, 10, 11},
    };
    System.out.println("A:");
    print(a);
    int[][] b = clockwise(a);
    System.out.println("B:");
    print(b);
  }
  */
  
  //////////////////
  //  problem 7   //
  //////////////////
  /*
  public static int[] increasing(){
    Scanner S = new Scanner(System.in);
    int[] a = new int[10];
    System.out.println("Please enter 10 ascending integers:");
    for(int i = 0; i < a.length; i++){
      if(S.hasNextInt()){
        int x = S.nextInt();
        a[i] = x;
      }
      else{
        System.out.println("Error! Please enter an integer:");
        i--;
      }
      if((i > 0) && (a[i] <= a[i-1])){
        System.out.println("Error! Input must be greater than last integer:");
        i--;
      }
      S.nextLine();
    }
    return a;
  }
  public static void main(String[] args){
    int[] a = increasing();
    System.out.print("{");
    for(int i = 0; i < a.length; i++){
      System.out.print(a[i]+" ");
    }
    System.out.print("}\n");
  }
  */
  
  //  ~   //
  // SHED //
  //  ~   //
  
  public static void binary(int[] a, int val){
    int start = 0;
    int end = a.length-1;
    int count = 1;
    while(start <= end){
      int mid = (start + end)/2;
      if(a[mid] < val){
        start = mid + 1;
      }
      if(a[mid] > val){
        end = mid - 1;
      }
      if(a[mid] == val){
        System.out.println("Found "+val+" in "+count+" iterations.");
        break;
      }
      count++;
    }
    if(start > end){
      System.out.println(val+" was not found in "+count+"iterations.");
    }
  }
  public static void sort(int[] a){
    
    for(int i = a.length-1; i >= 1; i--){
      int maxVal = a[0];
      int maxIndex = 0;
      for(int j = 1; j <= i; j++){
        if(a[j] > maxVal){
          maxVal = a[j];
          maxIndex = j;
        }
      }
      if(maxIndex != i){
        a[maxIndex] = a[i];
        a[i] = maxVal;
      }
    }
  }
  public static void print(int[] a){
    System.out.print("{");
    for(int i = 0; i < a.length; i++){
      System.out.print(a[i]+" ");
    }
    System.out.print("}\n");
  }
  
  public static void main(String[] args){
    Scanner S = new Scanner(System.in);
    System.out.println("\nEnter the number of integers in the array:");
    int size = S.nextInt();
    S.nextLine();
    int[] a = new int[size];
    System.out.println("\nEnter the members of the array:");
    for(int i = 0; i < size; i++){
      a[i] = S.nextInt();
      S.nextLine();
    }
    print(a);
    sort(a);
    print(a);
    System.out.println("\nPlease enter the value you wish to search for: ");
    int val = S.nextInt();
    S.nextLine();
    binary(a, val);
  }
}