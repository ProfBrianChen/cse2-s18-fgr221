/*
Griffin Reichert
hw10 Robot City
4/23/17
*/

import java.util.Random;
public class RobotCity{
  public static int[][] buildCity(){
    Random random = new Random();
    int northSouthDimension = random.nextInt(6)+10; //randomizes number of city blocks running from north to south
    int eastWestDimension = random.nextInt(6)+10;   //randomizes number of city blocks running from east to west
    System.out.println("\nWelcome to your city!");
    System.out.println("North-South: "+northSouthDimension+"\tEast-West: "+eastWestDimension+"\n");
    int[][] cityArray = new int[northSouthDimension][eastWestDimension]; //new array of specified dimensions
    for (int NS = 0; NS < cityArray.length; NS++){      //number of rows
      for (int EW = 0; EW < cityArray[0].length; EW++){ //members of each row
        int population = random.nextInt(900)+100;       //randomly sets population of each block to an integer between 100-999
        cityArray[NS][EW] = population;
      }
    }
    return cityArray;
  }
  
  public static void display(int[][] array){ //method to print array
    for(int i = 0; i < array.length; i++){
      for(int j = 0; j < array[i].length; j++){
        int val = array[i][j];
        System.out.printf("%5d",val);        //uses print with format to format all numbers in spreadsheet format
      }
      System.out.print("\n");
    }
    System.out.print("\n");
  }
  
  public static void invade(int[][] array, int robotTotal){ //method to invade the city with robots
    Random random = new Random();  
    int robotCount = 0;
    int roboPerRow = (int)(robotTotal/array.length);  //esimates how many robots should go in each row
    System.out.println("OH NO, ROBOTS ARE INVADING!");
    for(int i = 0; i < array.length; i++){
        for(int roboInRow = 0; roboInRow <= roboPerRow; roboInRow++){
          boolean flag = false;
          if(robotCount < robotTotal){
            do{
              int r = random.nextInt(array[0].length);
              int val = array[i][r];
              if( val > 0 ){
                array[i][r] = val*(-1);
                flag = true;
                robotCount++;
              }
            }while( flag == false );
          }
      }
    }
//    System.out.println("Landed: "+robotCount);
  }//method
  
  public static void update(int[][] array){
    for(int u = 0; u < 5; u++){
      
      for(int i = 0; i < array.length; i++){
        for(int j = 0; j < array[i].length; j++){
          if(j == array[i].length - 1){
            int prev = array[i][j-1];
            int val = array[i][j];
            
            if(prev < 0){
              if( val > 0 ){
                array[i][j] = val*(-1);
              }
            }
            else if(prev > 0){
              if( val < 0 ){
                array[i][j] = val*(-1);
              }
            }
          }
          if( j < array[i].length - 1){  
            int next = array[i][j+1];
            int val = array[i][j];
            if(val < 0){
              array[i][j] = val*(-1);
              if(next > 0){
                array[i][j+1] = next*(-1);
                j++;
              }
              else if(next < 0){
                j++;
              }
            }  
          }
          
        }//for j
      }//for i
      System.out.println("Update "+(u+1)+":");
      display(array);
    }
    
  }//method
  
  public static void main(String[] args){
    Random random = new Random();
    int[][] cityArray = buildCity();
    display(cityArray);
    int robots = random.nextInt(10)+20;
    invade(cityArray, robots); 
    System.out.println("Number of invading robots: "+robots+"\n");
    display(cityArray);
    update(cityArray);
    
  }
}