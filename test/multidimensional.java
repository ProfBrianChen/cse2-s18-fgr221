

public class multidimensional{
  public static void printRowMajor(int[][] array){
    for(int i = 0; i < array.length; i++){
      for(int j = 0; j < array[i].length; j++){
        System.out.print(array[i][j] + " ");
      }
      System.out.print("\n");
    }
  }
  public static void printColumnMajor(int[][] array){
    for(int i = 0; i < array[i].length; i++){
      for(int j = 0; j< array.length; j++){
        System.out.print(array[j][i] + " ");
      }
      System.out.print("\n");
    }
  }
  public static void main(String[] args){
    int[][] rowList = {
      {11, 22, 33},
      {44, 55},
      {66, 77, 88, 99},
    };
    int[][] colList = {
      {1, 6, 11},
      {2, 7, 12},
      {3, 8, 13},
      {4, 9, 14},
      {5, 10, 15},
    };
    printRowMajor(rowList);
    System.out.print("\n");
    printColumnMajor(colList);

  }
}