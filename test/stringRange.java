public class stringRange{
  
  public static String[] range(String[] a){
    int min = 100;
    int max = 0;
    int val = 0;
    String [] range = new String[2];
    for(int i = 0; i < a.length; i++){
//       String s = a[i];
      val = a[i].length();
      if(val < min){
        range[0] = a[i];
        min = val;
      }
      if(val > max){
        range[1] = a[i];
        max = val;
      }
    }
    
    return range;
  }
  public static void main(String[] args){
    String[] x = { "one", "two", "to", "fifteen", "three" };
    String[] y = range(x);
    System.out.print("[ ");
    for (int i = 0; i < y.length; i++){
      System.out.print(y[i]+" ");    
    }
    System.out.println("] \n");
  }
  
}