//binary search


public class binary{
  public static void sort(int[] list) {           //method to sort array
    for (int i = list.length - 1; i >=1; i--) {   
      int maxVal = list[0]; 
      int maxIndex = 0;                           //max index
      for (int j = 1; j <= i; j++) {              
        if (maxVal < list[j]) {
          maxVal = list[j];
          maxIndex = j;
        }
      }
      if (maxIndex != i) {
        list[maxIndex] = list[i];
        list[i] = maxVal;
      }
    }
  }
  public static void binarySearch(int[] list, int search){
    int start = 0;
    int stop = list.length;
    
    while(start < stop){
      int mid = (stop - start)/2 + start;
      if(list[mid] == search){
        System.out.println("Found the value: "+search);
        break;
      }
      if (search > list[mid]){      //
        start = mid + 1;            //updates bottom of range
      }
      if (search < list[mid]){
        stop = mid - 1;             //updates top of range
      }
    }
    
  }
  
  public static void main(String[] args){
    int[] list = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    binarySearch(list, 8);
    
  }
}
