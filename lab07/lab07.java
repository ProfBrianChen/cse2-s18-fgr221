/* 
Griffin Reichert
lab 07
3/30/18
*/

import java.util.Scanner; //imports scanner
import java.util.Random;  //imports random

public class lab07{ 
  public static String adjective(){ //method to return a random adjective
    Random randomGenerator = new Random(); //creates a new random object
    int x = randomGenerator.nextInt(10); //generates a random integer between 0-9
    String adjString = " "; //initializes a new string
    switch (x){  //switch statement that sets string to one of the adjectives determined by the random number generated
      case 0:
        adjString = "quick";
        break;
      case 1:
        adjString = "slow";
        break;
      case 2:
        adjString = "fast";
        break;
      case 3:
        adjString = "big";
        break;
      case 4:
        adjString = "small";
        break;
      case 5:
        adjString = "lazy";
        break;
      case 6:
        adjString = "happy";
        break;
      case 7:
        adjString = "silly";
        break;
      case 8:
        adjString = "friendly";
        break;
      case 9:
        adjString = "scary";
        break;
    }
    return(adjString); //returns the string generated in the switch statement
  }
  public static String verb(){
    Random randomGenerator = new Random(); //creates a new random object
    int x = randomGenerator.nextInt(10); //generates a random integer between 0-9
    String verbString = " "; //initializes a new string
    switch (x){  //switch statement that sets string to one of the verbs determined by the random number generated
      case 0:
        verbString = "jumped over";
        break;
      case 1:
        verbString = "leaped";
        break;
      case 2:
        verbString = "ate";
        break;
      case 3:
        verbString = "passed";
        break;
      case 4:
        verbString = "saw";
        break;
      case 5:
        verbString = "knew";
        break;
      case 6:
        verbString = "liked";
        break;
      case 7:
        verbString = "found";
        break;
      case 8:
        verbString = "hugged";
        break;
      case 9:
        verbString = "joined";
        break;
    }
    return(verbString); //returns the string generated in the switch statement
  }
  public static String object(){
    Random randomGenerator = new Random(); //creates a new random object
    int x = randomGenerator.nextInt(10); //generates a random integer between 0-9
    String objString = " "; //initializes a new string
    switch(x){  //switch statement that sets string to one of the nouns determined by the random number generated
      case 0:
        objString = "dog";
        break;
      case 1:
        objString = "house";
        break;
      case 2:
        objString = "man";
        break;
      case 3:
        objString = "woman";
        break;
      case 4:
        objString = "car";
        break;
      case 5:
        objString = "tree";
        break;
      case 6:
        objString = "teacher";
        break;
      case 7:
        objString = "flower";
        break;
      case 8:
        objString = "bear";
        break;
      case 9:
        objString = "cat";
        break;
    }
    return(objString); //returns the string generated in the switch statement
  }
  public static String subject(){
    Random randomGenerator = new Random(); //creates a new random object
    int x = randomGenerator.nextInt(10); //generates a random integer between 0-9
    String subjString = "adj"; //initializes a new string
    switch (x){  //switch statement that sets string to one of the nouns determined by the random number generated
      case 0:
        subjString = "fox";
        break;
      case 1:
        subjString = "dog";
        break;
      case 2:
        subjString = "president";
        break;
      case 3:
        subjString = "athlete";
        break;
      case 4:
        subjString = "student";
        break;
      case 5:
        subjString = "businessman";
        break;
      case 6:
        subjString = "teacher";
        break;
      case 7:
        subjString = "fireman";
        break;
      case 8:
        subjString = "chef";
        break;
      case 9:
        subjString = "priest";
        break;
    }
    return(subjString); //returns the string generated in the switch statement
  }
  public static void sentence1(String subject){
    System.out.println("The "+adjective()+" "+adjective()+" "+subject+" "+verb()+" the "+adjective()+" "+object()+".");
  }
  public static void sentence2(String subject){
    System.out.println("It "+verb()+" the "+ object()+" until the "+adjective()+" "+object()+" "+verb()+".");
  }
  public static void sentence3(String subject){
    System.out.println("Suddenly the "+subject+"'s "+adjective()+" "+object()+" "+verb()+" the "+object()+".");
  }
  public static void sentence4(String subject){
    System.out.println("But the "+adjective()+" "+subject+" "+verb()+" the "+" " + adjective() +" "+ object()+".");
  }
  public static void paragraph(int count){
    Random randomGenerator = new Random(); //creates a new random object
    String subject = subject();
    int randSentence = 0;
    System.out.println("A long time ago in a galaxy far away... lived a " + subject + ".\n");
    sentence1(subject);
    for (int i = 0; i < (count - 1); i++){
      randSentence = randomGenerator.nextInt(3);
      switch (randSentence){
        case 0:
          sentence2(subject);
          break;
        case 1:
          sentence3(subject);
          break;
        case 2:
          sentence4(subject);
          break;
      }
    }
    System.out.println("\nAnd the " + adjective() + " " + subject + " lived happily ever after!    ~The End");
  }
  
  public static void main(String [] args){
    Scanner scanner = new Scanner(System.in);
    int sentenceNum = 0;
    while (1 == 1){
      System.out.println("How many sentences would you like in your paragraph? (Please enter an int):");
      if (scanner.hasNextInt()){
        sentenceNum = scanner.nextInt();
        break;
      }
      scanner.nextLine();
    }
    paragraph(sentenceNum);
  }
}